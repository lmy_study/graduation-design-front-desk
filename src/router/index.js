import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login/Login.vue'
import Register from '@/views/Register/Register'
import Forget from '@/views/Forget/Forget'
import Product from '@/views/Product/ProductList'
import ProductDetail from '@/views/Product/ProductDetail'
import Car from '@/views/Car/Car'
import OrderPreview from '@/views/Order/OrderPreview'
import OrderPay from '@/views/Order/OrderPay'
import Center from '@/views/Center/Center'
import About from '@/views/About/About'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/reg',
    name: 'Register',
    component: Register
  },
  {
    path: '/forget',
    name: 'Forget',
    component: Forget
  },
  {
    path: '/product',
    name: 'Product',
    component: Product
  },
  {
    path: '/productDetail',
    name: 'ProductDetail',
    component: ProductDetail
  },
  {
    path: '/car',
    name: 'Car',
    component: Car,
    meta:{
      isLogin:true    // 添加该字段，表示进入这个路由是需要登录的
    }
  },
  {
    path: '/order_preview',
    name: 'OrderPreview',
    component: OrderPreview,
    meta:{
      isLogin:true    // 添加该字段，表示进入这个路由是需要登录的
    }
  },
  {
    path: '/order_pay',
    name: 'OrderPay',
    component: OrderPay,
    meta:{
      isLogin:true    // 添加该字段，表示进入这个路由是需要登录的
    }
  },
  {
    path: '/center',
    name: 'Center',
    component: Center,
    meta:{
      isLogin:true    // 添加该字段，表示进入这个路由是需要登录的
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
function isNull(arg1)
{
 return !arg1 && arg1!==0 && typeof arg1!=="boolean"?true:false;
}
router.beforeEach((to,from,next)=>{
  if(to.matched.some(res=>res.meta.isLogin)){//判断是否需要登录
      var token = localStorage.getItem('token')
      if (token == '' || isNull(token)) {
          next({
              path:"/login",
              query:{
                  redirect:to.fullPath
              }
          });
      }else{
          next();
      }
  }else{
      next()
  }
});
export default router
